---
layout: archive
title: "Meet the team"
permalink: /team/
author_profile: true
---

Leo Wandersleb
--------------

<div class="author__avatar">
<img src="/images/leo.jpg" class="author__avatar" alt="photo of Leo Wandersleb">
</div>

Leo is "into Bitcoin" since 2012 and since October 2015 a Bitcoin developer for the
[Mycelium Bitcoin Wallet](https://play.google.com/store/apps/details?id=com.mycelium.wallet).
He holds a diploma in Mathematics from [TU München](https://www.tum.de/), was a
game developer for [Travian](https://www.traviangames.com/en/), did
[some attempts](https://play.google.com/store/apps/details?id=de.leowandersleb.fluxcards)
at doing his own apps and [contributed](https://github.com/Giszmo/)
to several open source projects.


Kristina Tezieva
----------------

<div class="author__avatar">
<img src="/images/kristina.jpg" class="author__avatar" alt="photo of Kristina Tezieva">
</div>
