---
title: "Exodus Crypto Wallet"
altTitle: 

users: 100000
appId: exodusmovement.exodus
launchDate: 2019-06-06
latestUpdate: 2020-01-30
apkVersionName: "20.1.30"
stars: 4.8
ratings: 4619
reviews: 1740
size: 34M
website: https://www.exodus.io/mobile
repository: https://github.com/exodusmovement
issue: 
icon: exodusmovement.exodus.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-12-20
reviewStale: true
reviewArchive:


internalIssue: 
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/exodusmovement.exodus/
redirect_from:
  - /exodusmovement.exodus/
---


This app certainly sounds like it is non-custodial:

> Don’t give control of your private keys to centralized wallets and exchanges
that can suffer from hacks and lose your funds. Exodus encrypts your private
keys and transaction data on your device so that no one can access your
cryptocurrency but you. You can also Enable Face or Touch ID to conveniently
secure your wallet without having to type your passcode.

Is it open source though? ...

No word about a repository on the description but the website links to
[this GitHub](https://github.com/ExodusMovement) but there is no repository name
suggesting there to be an Android wallet and as none of the company's
repositories contains the appId or at least [GitHub can't find it](https://github.com/search?q=org%3AExodusMovement+%22exodusmovement.exodus%22&type=Code),
we conclude that this app is closed source.

**Update**: We asked them about the source code but
[they confirmed](https://twitter.com/exodus_io/status/1208416689672663046):
Exodus [is and stays closed source](https://support.exodus.io/article/89-is-exodus-open-source).

Our verdict: This app is **not verifiable**.
