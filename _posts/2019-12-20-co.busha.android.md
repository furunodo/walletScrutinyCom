---
title: "Busha: Buy & Sell Bitcoin, Ethereum. Crypto Wallet"
altTitle: 

users: 5000
appId: co.busha.android
launchDate: 2019-01-21
latestUpdate: 2020-01-24
apkVersionName: "2.3.3"
stars: 3.9
ratings: 58
reviews: 48
size: 19M
website: https://busha.co
repository: 
issue: 
icon: co.busha.android.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-12-20
reviewStale: true
reviewArchive:


internalIssue: 71
providerTwitter: getbusha
providerLinkedIn: 
providerFacebook: getbusha
providerReddit: 

permalink: /posts/co.busha.android/
redirect_from:
  - /co.busha.android/
---


The description

> Won’t you rather trade and store your crypto assets on a platform you can
  trust? Busha is a Nigerian based crypto exchange that offers you all these and
  more.

sounds like it's an app to access an account on a custodial platform.

And there is nothing on the website indicating otherwise.

Our verdict: **not verifiable**.