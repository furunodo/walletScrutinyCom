---
title: "Luno: Buy Bitcoin, Ethereum and Cryptocurrency"
altTitle: 

users: 1000000
appId: co.bitx.android.wallet
launchDate: 2014-11-01
latestUpdate: 2020-02-13
apkVersionName: "6.6.0"
stars: 4.1
ratings: 24590
reviews: 14040
size: 10M
website: https://www.luno.com/
repository: 
issue: 
icon: co.bitx.android.wallet.png
bugbounty: 
verdict: nosource # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty
date: 2019-11-14
reviewStale: true
reviewArchive:


internalIssue: 
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/2019/11/luno/
redirect_from:
  - /luno/
  - /co.bitx.android.wallet/
---


Luno: Buy Bitcoin, Ethereum and Cryptocurrency
advertises on the Playstore:

> **Keeping your crypto safe**<br>
> The majority of customer Bitcoin funds are kept in what we call “deep freeze” storage. These are multi-signature wallets, with private keys stored in different bank vaults. No single person ever has access to more than one key. We maintain a multi-signature hot wallet to facilitate instant Bitcoin withdrawals.

This tells us we are dealing with a custodial wallet here. The security **cannot
be verified**.
